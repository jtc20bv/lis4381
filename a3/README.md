# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - A3 Read Me

## Jacob Carleton

### Assignment 3

> 

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](img/PSERD.png)


*Screenshot of running application’s first & second user interface*:


| First User Interface | Second User Interface  |
| -----------------------------------| ----------------------------------- |
| ![first user interface](img/First_User_Interface.png) | ![second user interface](img/Second_User_Interface.png)|



*Screenshot of 10 records for each table*:

![petstore](img/petstore.png)
![pet](img/pet.png)
![customer](img/customer.png)




#### Skill Set Links:

*Skill Set Locations:*
    [Skill Set 4](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss4/ "Skill Set 4")
    ![SS4](img/ss4.png)
    [Skill Set 5](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss5/ "Skill Set 5")
    ![SS5](img/ss5.png)
    [Skill Set 6](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss6/ "Skill Set 6")
    ![SS6](img/ss6.png)



#### mySQL Links:
*Sql Links:*

[a3.mwb](https://bitbucket.org/jtc20bv/lis4381/src/master/a3/docs/a3.mwb "a3.mwb")

[a3.sql](https://bitbucket.org/jtc20bv/lis4381/src/master/a3/docs/a3.sql "a3.sql")