# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - P1 Read Me

## Jacob Carleton

### Project 1

> 

#### Assignment Screenshots:



*Screenshot of running application’s first & second user interface*:


| First User Interface | Second User Interface  |
| -----------------------------------| ----------------------------------- |
| ![first user interface](img/First_User_Interface.png) | ![second user interface](img/Second_User_Interface.png)|






#### Skill Set Links:

*Skill Set Locations:*

[Skill Set 7](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss7/ "Skill Set 7")

![SS7](img/ss7.png)

[Skill Set 8](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss8/ "Skill Set 8")

![SS8](img/ss8.png)

[Skill Set 9](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss9/ "Skill Set 9")

![SS9](img/ss9.png)


