
# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - A1 Read Me

## Jacob Carleton

### Assignment 1


> #### Git commands w/short descriptions:

1. git init - creates a new Git repository 
2. git status - displays the state of the repository
3. git add - adds a change in the working directory 
4. git commit - used to move files from the staging area to a commit 
5. git push - upload local repository content to a remote repository
6. git pull - used to fetch and download content from a remote repository 
7. One additional git command (git merge) - To merge a different branch into your active branch

#### Assignment Screenshots:

*Screenshot of AMPPS running [MY PHP INFO](http://localhost/cgi-bin/phpinfo.cgi)

![AMPPS Installation Screenshot](img/ampps.png)


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/helloworld.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


