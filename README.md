# LIS4381 - MOBILE WEB APPLICATION DEVELOPMENT

## Jacob Carleton

### Assignment's Read Me Files:

1. [A1 README.md](https://bitbucket.org/jtc20bv/lis4381/src/master/a1/README.md)
	* Install AMPPS
	* Install JDK
	* Install Android Studio and create My First App
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorials 
	* Provide Git command descriptions
	
2. [A2 README.md](https://bitbucket.org/jtc20bv/lis4381/src/master/a2/README.md)
	* Screenshots of Android Studio "Healthy Recipe" App
	* Skill Sets 1-3

3. [A3 README.md](https://bitbucket.org/jtc20bv/lis4381/src/master/a3/README.md)
	* Screenshot of ERD
	* Screenshots of Concert tickets first and second user interface
	* Links to a3 sql and mwb
	* Skill Sets 4-6

4. [P1 README.md](https://bitbucket.org/jtc20bv/lis4381/src/master/p1/README.md)
	* Creating a business card application using Android Studio
	* Includes Launcher Icon, selfie and color changes.
	* Skill Sets 7-9

5. [A4 README.md](https://bitbucket.org/jtc20bv/lis4381/src/master/a4/README.md)
	* Create Online Portfolio using Apache and Bootstrap
	* Link to local host web app
	* Skill Sets 10-12






