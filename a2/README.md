
# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - A2 Read Me

## Jacob Carleton

### Assignment 2

> 

#### Assignment Screenshots:


*Screenshot of running application’s first user interface*:

![first user interface](img/First_User_Interface.png)

*Screenshot of running application’s second user interface*:

![second user interface](img/Second_User_Interface.png)

#### Skill Set Links:

*Skill Set Locations:*
    [Skill Set 1](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss1/ "Skill Set 1")
    [Skill Set 2](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss2/ "Skill Set 2")
    [Skill Set 3](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss3/ "Skill Set 3")

