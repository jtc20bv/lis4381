<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jacob Carleton">
    <link rel="icon" href="favicon.ico">

		<title>Lis 4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: Screenshots of Android Studio "Healthy Recipe" App</strong> 
				</p>

				<h4>Java Installation</h4>
				<img src="img/First_User_Interface.png" class="img-responsive center-block" alt="*Screenshot of running application’s first user interface*:">

				<h4>Android Studio Installation</h4>
				<img src="img/Second_User_Interface.png" class="img-responsive center-block" alt="*Screenshot of running application’s second user interface*:">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
