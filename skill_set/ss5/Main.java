import java.util.*;

public class Main {
  public static void main(String[] args) {
    System.out.println("Developer: Jacob Carleton");
    System.out.println("Date: 10/06/2021");
    System.out.println("Program searches user-entered integer w/in array of integers.");
    System.out.println("Creates an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");

    Methods.nestedStructure();

  }
}
