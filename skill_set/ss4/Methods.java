import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Jacob Carleton");
        System.out.println("Program Evaluates User-Entered Characters");
        System.out.println("Use The Following Characters: W or w, C or c, H or h, N or n.");
        System.out.println("Use The Following Decesion Structures: if...else, and switch.");

        System.out.println();
    }

    public static void getUserPhoneType()
    {
       String myStr="";
       char myChar=' '; 
       Scanner sc = new Scanner(System.in);
       
    

       System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");

       System.out.print("Enter Phone Type: ");
       myStr = sc.next().toLowerCase();
       myChar = myStr.charAt(0);

       System.out.println("\nif...else:");

       if (myChar == 'w')
          System.out.println("Phone Type: work");
       else if (myChar == 'c')  
          System.out.println("Phone Type: cell");
       else if (myChar == 'h')
          System.out.println("Phone Type: home");
       else if (myChar == 'n')
          System.out.println("Phone Type: none");
       else 
          System.out.println("Incorrect Character Entry");

       System.out.println();
       System.out.println("switch:");
       switch (myChar)
       {
       case 'w':
         System.out.println("Phone Type: work");
         break;
       case 'c':
         System.out.println("Phone Type: cell");
         break;
       case 'h':
         System.out.println("Phone Type: home");
         break;
       case 'n':
         System.out.println("Phone Type: none");
         break;
       default:
         System.out.println("Incorrect Character Entry.");
         break;
       }
   }
}



