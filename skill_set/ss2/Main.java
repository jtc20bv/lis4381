import java.util.Scanner;


class Main
{
    public static void main(String args[])
    {
        Methods.getRequirements();
        System.out.println("***Call Static (No Object) Void (Non-Value Returning) Method.***");
            Methods.largestNumber();

            System.out.println("***Call Static (No Object) Value-Returning Method and Void Method.***");
            int myNum1=0, myNum2 = 0;


            System.out.print("Enter First Integer: ");
            myNum1 = Methods.getNum();

            System.out.print("Enter Second Integer: ");
            myNum2 = Methods.getNum();

            Methods.evaluateNumber(myNum1,myNum2);

    

    }
}

