import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Jacob Carleton");
        System.out.println("Program Evaluates Integers as Even or Odd");
        System.out.println("*Note: Program does *NOT* Check For Non-Numeric Characters or Non-Integer Values.");
        System.out.println();
    }

    public static void largestNumber()
    {

        int num1, num2;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter First Integer: ");
        num1 = sc.nextInt();

            System.out.print("Enter Second Integer: ");
            num2 = sc.nextInt();

            System.out.println();
            if (num1 > num2)
            System.out.println(num1 + " is larger than " + num2);
            else if (num2 > num1)
            System.out.println(num2 + " is larger than " + num1);
            else
            System.out.println("Integers Are Equal.");
    }

    public static int getNum()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    public static void evaluateNumber(int num1, int num2)
    {
        System.out.println();
        if (num1 > num2)
        System.out.println(num1 + " is larger than " + num2);
        
    }
}



