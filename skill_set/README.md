# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - Skill Set(s) Read Me

## Jacob Carleton

### Skill Set(s)



#### Skill Set Screenshots:


*Screenshot of Skill Set 1*:

![Skill Set 1](ss1/SkillSet1.png)

*Screenshot of Skill Set 2*:

![Skill Set 2](ss2/SkillSet2.png)

*Screenshot of Skill Set 3*:

![Skill Set 3](ss3/SkillSet3.png)

*Screenshot of Skill Set 4*:

![Skill Set 4](ss4/ss4.png)

*Screenshot of Skill Set 5*:

![Skill Set 5](ss5/ss5.png)

*Screenshot of Skill Set 6*:

![Skill Set 6](ss6/ss6.png)

*Screenshot of Skill Set 7*:

![Skill Set 7](ss7/ss7.png)

*Screenshot of Skill Set 8*:

![Skill Set 8](ss8/ss8.png)

*Screenshot of Skill Set 9*:

![Skill Set 9](ss9/ss9.png)

*Screenshot of Skill Set 10*:

![Skill Set 10](ss10/ss10.png)

*Screenshot of Skill Set 11*:

![Skill Set 11](ss11/ss11.png)

*Screenshot of Skill Set 12*:

![Skill Set 12](ss12/ss12.png)





