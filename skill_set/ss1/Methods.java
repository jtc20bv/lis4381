import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Jacob Carleton");
        System.out.println("Program Evaluates Integers as Even or Odd");
        System.out.println("*Note: Program does *NOT* Check For Non-Numeric Characters.");
        System.out.println();
    }

    public static void evaluateNumber()
    {

        int x = 0;
        System.out.print("Enter Integer: ");
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();

        if (x % 2 == 0)
    {
            System.out.print(x + " is an even number.");
    }
        else
    {
        System.out.print(x + " is an odd number.");
    }

    }
}


    
