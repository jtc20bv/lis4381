# LIS4381 (MOBILE WEB APPLICATION DEVELOPMENT) - A4 Read Me

## Jacob Carleton

### Assignment 4


#### Requirements:

1. Create Online Portfolio using Apache and Bootstrap
2. Use jQuery to validate client side data
3. Link to local host web app
4. Skill Sets 10-12


#### Assignment Screenshots:

*Link to Local LIS4381 web app*

[A4 Local LIS4381 web app](http://localhost/repos/src/a4/index.php)


#### Screenshot of Portfolio:

| Home |  Pre-Data Validation | Passed Validation |
| -----------------------------------| ----------------------------------- |  ----------------------------------- |
| ![A4 Screenshot 1](img/a4a.png) | ![A4 Screenshot 2](img/a4b.png) | ![A4 Screenshot 3](img/a4c.png) |

#### Skill Set Links:

*Skill Set Locations:*
    [Skill Set 10](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss10/ "Skill Set 10")
    ![SS10](img/ss10.png)
    [Skill Set 11](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss11/ "Skill Set 11")
    ![SS11](img/ss11.png)
    [Skill Set 12](https://bitbucket.org/jtc20bv/lis4381/src/master/skill_set/ss12/ "Skill Set 12")
    ![SS12](img/ss12.png)

